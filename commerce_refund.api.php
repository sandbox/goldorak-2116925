<?

/**
 * @defgroup commerce_refund Refund API
 * @{
 * API for extending Drupal Commerce payment framework with refund functionality.
 */

/**
 * Define payment methods available to the Commerce Payment framework.
 *
 * The Payment module uses this hook to gather information on payment methods
 * defined by enabled modules.
 *
 * Payment methods depend on a variety of callbacks that are used to configure
 * the payment methods via Rules actions, integrate the payment method with the
 * checkout form, handle display and manipulation of transactions after the fact,
 * and allow for administrative payment entering after checkout. The Payment
 * module ships with payment method modules useful for testing and learning, but
 * all integrations with real payment providers will be provided as contributed
 * modules. The Payment module will include helper code designed to make different
 * types of payment services easier to integrate as mentioned above.
 *
 * Each payment method is an associative array with the following keys:
 *  - method_id: string identifying the payment method (must be a valid PHP
 *    identifier).
 *  - base: string used as the base for callback names, each of which
 *    will be defaulted to [base]_[callback] unless explicitly set; defaults
 *    to the method_id if not set.
 *
 * @return
 *   An array of payment methods, using the format defined above.
 */
function hook_commerce_refund_terminal_info() {
  $refund_terminals['commerce_paypal_wps'] = array(
    'base' => 'commerce_paypal_wps',
    'method_id' => 'paypal_wps'
  );

  return $refund_terminals;
}

/**
 * Alter payment methods defined by other modules.
 *
 * This function is run before default values have been merged into the payment
 * methods.
 *
 * @param $payment_methods
 *   An array of payment methods, keyed by method id.
 */
function hook_commerce_refund_terminal_info_alter(&$refund_terminals) {
// No example.
}

/**
 * Payment method callback; return the settings form for a payment method.
 *
 * @param $settings
 *   An array of the current settings.
 *
 * @return
 *   A form snippet.
 */
function CALLBACK_commerce_refund_terminal_form($form, &$form_state, $order) {
// No example.
}

/**
 * Payment method callback; return the settings form for a payment method.
 *
 * @param $settings
 *   An array of the current settings.
 *
 * @return
 *   A form snippet.
 */
function CALLBACK_commerce_refund_terminal_form_validate($form, &$form_state, $order) {
// No example.
}

/**
 * Payment method callback; return the settings form for a payment method.
 *
 * @param $settings
 *   An array of the current settings.
 *
 * @return
 *   A form snippet.
 */
function CALLBACK_commerce_refund_terminal_form_submit($form, &$form_state, $order) {
// No example.
}