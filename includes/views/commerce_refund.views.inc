<?php

/**
 * @file
 * Export the refund functionality to commerce payment view.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_refund_views_data_alter(&$data) {
  $data['commerce_payment_transaction']['refund'] = array(
    'title' => t('Refund process'),
    'help' => t('Display a refund button to reimburse customers.'),
    'area' => array(
      'handler' => 'commerce_refund_handler_area_refund_process',
    ),
  );
}
